import 'package:app/bloc/word_bloc.dart';
import 'package:app/screens/list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController _word = TextEditingController();
   var _bloc;

  @override

  void initState() {
    _bloc = BlocProvider.of(context);
    super.initState();
  }
  @override

  void dispose() {
   _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.blueGrey[900],
        body: WordPage(context,_word,_bloc),
      ),
    );
  }
}

Widget WordPage(BuildContext context,TextEditingController word,_bloc) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 10),
    child: Column(
      children: [
       Spacer(),
       Container(
         child: Text('Dictionary app',style: TextStyle(
           color: Colors.deepOrangeAccent,
           fontSize: 34,
           fontWeight: FontWeight.w800
         ),),
       ),
      SizedBox(height: 30,),
      Container(
        child: TextField(
          controller: word,
          decoration: InputDecoration(
            hintText: "Search a word ...",
            fillColor: Colors.grey[100],
            filled:true,
            prefixIcon: Icon(Icons.search),
            hintStyle: TextStyle(
              color: Colors.grey
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                  color: Colors.red
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: Colors.transparent
              ),

            )
          ),
        ),
      ),
      Spacer(),
        Container(
          height: 60,
          child: ElevatedButton(
            onPressed: ()  {
              _bloc.add(GetWordEvent());
              Navigator.push(context, MaterialPageRoute(builder: (context) => List()));
            },
            child: Center(
              child: Text("Search",style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700
              ),),
            ),
          ),
        )
      ],
    ),
  );
}
