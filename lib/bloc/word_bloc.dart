import 'package:app/repository/word_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'word_event.dart';
part 'word_state.dart';

class WordBloc extends Bloc<WordEvent, WordState> {
  WordBloc() : super(WordInitial()) {
    on<WordEvent>((event, emit) async {
      final wordRepo = WordRepository();
      if (event is GetWordEvent) {
        emit(WordInitial());
        try {
          wordRepo.getWordRepo();
        } catch (e) {
          WordError(message: e);
        }
      }
    });
  }
}
