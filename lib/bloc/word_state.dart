part of 'word_bloc.dart';

@immutable
abstract class WordState {}

class WordInitial extends WordState {}
class WordLoading extends WordState {

}
class WordSuccess extends WordState {
final word;

WordSuccess({
  this.word
});
}
class WordError extends WordState {
final message;

WordError({this.message});
}
