import 'package:app/bloc/word_bloc.dart';
import 'package:flutter/material.dart';
import 'package:app/screens/homeScreen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {

  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Dictionary App',
      theme: ThemeData( // This is the theme of your application.
          primarySwatch: Colors.blue,
          splashFactory: InkSplash.splashFactory
      ),
      home: BlocProvider(
        create: (context) => WordBloc(),
        child: HomeScreen(),
      ),
    );
  }
}
